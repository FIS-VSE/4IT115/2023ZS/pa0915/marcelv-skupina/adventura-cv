package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.Batoh;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;

public class PanelBatohu implements Observer {

    private Batoh batoh = Hra.getSingleton().getBatoh();
    private FlowPane flowPane = new FlowPane();

    public PanelBatohu() {
        flowPane.setPrefWidth(200);
        flowPane.setPrefHeight(200);
        update();
        batoh.registerObserver(this);
    }

    public FlowPane getFlowPane() {
        return flowPane;
    }

    @Override
    public void update() {
        flowPane.getChildren().clear();
        ImageView imageView2 = new ImageView(new Image(HerniPlocha.class.getClassLoader().getResourceAsStream(
                "Troll-face.sh.png"), 50, 50, false, false));
        flowPane.getChildren().add(imageView2);

//        for (String nazev : batoh.getMnozinaVeci()) {
//            ImageView imageView = new ImageView(new Image(HerniPlocha.class.getClassLoader().getResourceAsStream(
//                    "veci/" + nazev + ".jpeg"), 50, 50, false, false));
//            flowPane.getChildren().add(imageView);
//        }
    }
}
