package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.web.WebView;

import java.util.concurrent.Executors;

public class Adventura extends Application {

    private ImageView imageViewKarkulky;
    private ImageView imageViewVlka;

    private final IHra hra = Hra.getSingleton();

    /***************************************************************************
     * Metoda, jejímž prostřednictvím se spouští celá aplikace.
     *
     * @param args parametry příkazového řádku
     */
    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("-gui")) {
            Application.launch();
        } else {
            IHra hra = Hra.getSingleton();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane borderPane = new BorderPane();
        HBox dolniPanel = new HBox();

        TextArea konzole = new TextArea();
        borderPane.setCenter(konzole);
        konzole.setText(hra.vratUvitani());
        konzole.setEditable(false);

        PanelVychodu panelVychodu = new PanelVychodu(konzole);
        borderPane.setRight(panelVychodu.getListView());

        PanelBatohu panelBatohu = new PanelBatohu();
        borderPane.setLeft(panelBatohu.getFlowPane());

        TextField uzivatelskyVstup = pripravUzivatelskyVstup(borderPane, dolniPanel, konzole);

        HerniPlocha herniPlocha = new HerniPlocha();
        borderPane.setTop(herniPlocha.getAnchorPane());

        MenuBar menuBar = pripravMenu();

        VBox vBox = new VBox();
        vBox.getChildren().addAll(menuBar, borderPane);

        imageViewVlka = herniPlocha.getImageViewVlka();
        imageViewKarkulky = herniPlocha.getImageViewKarkulky();

        imageViewKarkulky.setTranslateX(100);


        TranslateTransition translateTransition = new TranslateTransition();
        translateTransition.setNode(imageViewVlka);
        translateTransition.setByX(300);
        translateTransition.setByY(300);
        translateTransition.setCycleCount(-1);
        translateTransition.setDuration(javafx.util.Duration.seconds(5));
        translateTransition.play();

        RotateTransition rotateTransition = new RotateTransition();
        rotateTransition.setNode(imageViewVlka);
        rotateTransition.setDuration(javafx.util.Duration.seconds(5));
        rotateTransition.setByAngle(360);
        rotateTransition.setCycleCount(1000);
        rotateTransition.setAutoReverse(true);
        rotateTransition.play();

        ScaleTransition scaleTransition = new ScaleTransition();
        scaleTransition.setNode(imageViewVlka);
        scaleTransition.setDuration(javafx.util.Duration.seconds(5));
        scaleTransition.setByX(2);
        scaleTransition.setByY(2);
        scaleTransition.setCycleCount(1000);
        scaleTransition.setAutoReverse(true);
        scaleTransition.play();

        Scene scene = new Scene(vBox);
        uzivatelskyVstup.requestFocus();

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private MenuBar pripravMenu() {
        MenuBar menuBar = new MenuBar();
        Menu menuSoubor = new Menu("Soubor");
        Menu menuNapoveda = new Menu("Nápověda");
        menuBar.getMenus().addAll(menuSoubor, menuNapoveda);
        MenuItem itemNovaHra = new MenuItem("Nová hra");
        MenuItem itemKonec = new MenuItem("Konec");
        MenuItem itemNapoveda = new MenuItem("Nápověda");
        MenuItem itemOAplikaci = new MenuItem("O aplikaci");
        menuSoubor.getItems().addAll(itemNovaHra, itemKonec);
        menuNapoveda.getItems().addAll(itemNapoveda, itemOAplikaci);

        itemKonec.acceleratorProperty( ).set( javafx.scene.input.KeyCombination.keyCombination( "Ctrl+X" ) );
        itemKonec.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.exit(0);
            }
        });

        WebView webview = new WebView();
        webview.getEngine().load(
                getClass().getResource("/napoveda.html").toExternalForm()
        );
        itemNapoveda.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Stage stage = new Stage();
                stage.setScene(new Scene(webview));
                stage.show();
            }
        });

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("O aplikaci");
        alert.setHeaderText("Adventura");
        alert.setContentText("Toto je adventura vytvořená v rámci semestrální práce předmětu 4IT115.\n" +
                "Autor: Valdemar Xaver Matějka\n" +
                "Verze: 1.0");

        itemOAplikaci.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                alert.showAndWait();
            }
        });
        return menuBar;
    }

    private TextField pripravUzivatelskyVstup(BorderPane borderPane, HBox dolniPanel, TextArea konzole) {
        TextField uzivatelskyVstup = new TextField();
        borderPane.setBottom(dolniPanel);
        Label popisek = new Label("Zadej příkaz: ");
        popisek.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        popisek.setAlignment(Pos.CENTER);
        dolniPanel.getChildren().addAll(popisek, uzivatelskyVstup);
        dolniPanel.setAlignment(Pos.CENTER);
        uzivatelskyVstup.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String prikaz = uzivatelskyVstup.getText();
                String odpoved = hra.zpracujPrikaz(prikaz);
                konzole.appendText("\n" + odpoved + "\n");

                uzivatelskyVstup.clear();
            }
        });
        return uzivatelskyVstup;
    }
}
