package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class HerniPlocha implements Observer {

    private HerniPlan herniPlan = Hra.getSingleton().getHerniPlan();
    private AnchorPane anchorPane = new AnchorPane();

    private ImageView imageViewMapyHry;
    private ImageView imageViewKarkulky;
    private ImageView imageViewVlka;


    public HerniPlocha() {
        Image image = new Image(HerniPlocha.class.getClassLoader().getResourceAsStream("herniPlan.png"), 400, 250,
                false, false);
        imageViewMapyHry = new ImageView(image);

        Image imageKarkulky = new Image(HerniPlocha.class.getClassLoader().getResourceAsStream("karkulka.png"), 50, 50,
                false, false);
        imageViewKarkulky = new ImageView(imageKarkulky);

        Image imageVlka = new Image(HerniPlocha.class.getClassLoader().getResourceAsStream("vlk.png"), 50, 50,
                false, false);
        imageViewVlka = new ImageView(imageVlka);

        anchorPane.getChildren().addAll(imageViewMapyHry, imageViewKarkulky, imageViewVlka);
        aktualizuj();
        herniPlan.registerObserver(this);
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    public void aktualizuj() {
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        AnchorPane.setLeftAnchor(imageViewKarkulky, aktualniProstor.getPosLeft());
        AnchorPane.setTopAnchor(imageViewKarkulky, aktualniProstor.getPosTop());
    }

    @Override
    public void update() {
        aktualizuj();
    }

    public ImageView getImageViewKarkulky() {
        return imageViewKarkulky;
    }

    public ImageView getImageViewVlka() {
        return imageViewVlka;
    }
}